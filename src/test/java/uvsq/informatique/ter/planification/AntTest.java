package uvsq.informatique.ter.planification;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import uvsq.informatique.ter.planification.core.Competence;
import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.data.csv.DataSource;
import uvsq.informatique.ter.planification.data.csv.ResultOutputCSV;
import uvsq.informatique.ter.planification.heuristics.Heuristics;
import uvsq.informatique.ter.planification.heuristics.SimpleHeuristics;
import uvsq.informatique.ter.planification.optimization.AntColony;
import uvsq.informatique.ter.planification.optimization.Planification;
import uvsq.informatique.ter.planification.scheme.PSGS;

public class AntTest {
	
	DataSource source;
	SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	PSGS psgs;
	Resource targetResource;
	String filenameOutput = "src/main/resources/ant_test_output.txt";
	Date periodStart;
	Date periodEnd;
	
	@Before
	public void initialize() throws ParseException, IOException{
		source = new DataSource("src/main/resources/data_projects.csv",
				"src/main/resources/data_ressources.csv");
		ResultOutputCSV.writeInitialAvailability(source.getRessources(), "src/main/resources/before_PSGS/remain");
		periodStart = df.parse("01.12.2016");
		periodEnd = df.parse("30.06.2017");
		psgs = new PSGS(source.getTasks(), periodStart, periodEnd); 
		psgs.setOutput("src/main/resources/psgs_test_output.txt");
		Heuristics simple = new SimpleHeuristics();
		psgs.doPSGS(simple);
		for(Resource ress : source.getRessources()){
			if(ress.getCompetence().equals(Competence.CP_BI)){
				targetResource = ress;
				break;
			}
		}
		ResultOutputCSV.writeProjects(source.getTasks(), "src/main/resources/planified_projects.csv");
		ResultOutputCSV.writeRessources(source.getRessources(), "src/main/resources/after_PSGS/available");
		ResultOutputCSV.writeOccupancyRate(source.getRessources(), "src/main/resources/occupancy_rate_before_ant.csv");
	}
	
	@Test
	public void antTest() throws IOException{
		AntColony colony = new AntColony(psgs.getPlanifiedTasks(), 5, targetResource, filenameOutput);
		colony.doACO(100);
		
		for(Task t : psgs.getPlanifiedTasks()){
			t.applyDecision(colony.getBestSolution().getDecisionbyTask(t));
		}
		ResultOutputCSV.writeProjects(source.getTasks(), "src/main/resources/planified_projects_best_solution.csv");
		ResultOutputCSV.writeRessources(source.getRessources(), "src/main/resources/best_solution/available");
		ResultOutputCSV.writeOccupancyRate(source.getRessources(), "src/main/resources/occupancy_rate_after_ant.csv");
		
		Planification planif = new Planification(source.getTasks(), periodStart, periodEnd);
		//planif.setOutput("src/main/resources/reintegration_info.txt");
		planif.reintegrateDelayedProjects("src/main/resources/reintegration_psgs_output.txt");
		ResultOutputCSV.writeProjects(source.getTasks(), "src/main/resources/after_reintegration.csv");
		ResultOutputCSV.writeOccupancyRate(source.getRessources(), "src/main/resources/occupancy_rate_after_reintegration.csv");
		System.out.println("PSGS all tasks: " + psgs.getAllTasks().size());
		System.out.println("PSGS planified tasks: " + psgs.getPlanifiedTasks().size());
		System.out.println("Reintegration all tasks: " + planif.getPSGS().getAllTasks().size());
		System.out.println("Reintegration planified tasks: " + planif.getPSGS().getPlanifiedTasks().size());
		
	}
	
	

}
