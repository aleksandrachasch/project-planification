package uvsq.informatique.ter.planification;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import uvsq.informatique.ter.planification.core.Project;
import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.data.csv.ProjectData;
import uvsq.informatique.ter.planification.data.csv.ResourceData;
import uvsq.informatique.ter.planification.data.csv.ResultOutput;
import uvsq.informatique.ter.planification.data.csv.ResultOutputCSV;
import uvsq.informatique.ter.planification.heuristics.Heuristics;
import uvsq.informatique.ter.planification.heuristics.SimpleHeuristics;
import uvsq.informatique.ter.planification.scheme.PSGS;

public class AppTest {
	
	ProjectData data;
	ResourceData resourceData;
	PSGS psgs;
	SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	
	@Before
	public void initialize() throws ParseException, IOException{
		resourceData = new ResourceData("src/main/resources/data_ressources.csv");
		data = new ProjectData("src/main/resources/data_projects.csv", resourceData.getRessources());
		/*
		 * TODO: period start and period end are hard coded here
		 */
		psgs = new PSGS(data.getAllTasks(), df.parse("01.12.2016"), df.parse("01.06.2017"));
		psgs.setOutput("src/main/resources/result.txt");
	}
	
	@Ignore("Test succeds")
	@Test
    public void readProjects() throws ParseException{
		//data.readProjects();
		//for(Project p : data.allprojects){
		//	System.out.println(p.getName() + ", " + p.getPriority());
		//}
		//assertEquals(data.allprojects.size(),25);
		data.readTasks();
		System.out.println("Ressource size: " + data.getAllRessources().size());
		System.out.println("Projects size: " + data.getAllProjects().size());
		System.out.println("Task size: " + data.getAllTasks().size());
		for(Task t : data.getAllTasks()){
			System.out.println("ID: " + t.getId());
			System.out.println("Name: " + t.getName());
			System.out.println("Project: " + t.getProject().getName());
			System.out.println("StartDate: " + t.getInitialStart().toString());
			System.out.println("EndDate: " + t.getInitialEnd().toString());
			System.out.println("Initial duration: " + t.getInitialDuration());
			System.out.println("NB of ressources: " + t.getEffort().size());
			for(Resource r : t.getEffort().keySet()){
				System.out.println("Ressource " + r.getCompetence().name() 
						+ ": " + t.getEffort().get(r));
			}
			System.out.println("----------------");
			
		}
	}
	
	@Test
	public void dateFormat() throws ParseException{
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(df.parse("13.02.2017"));
		System.out.println(df.parse("13.02.2017").toString());
		Date date1 = cal.getTime();
		Date date2 = df.parse("26.03.2017");
		System.out.println(date2.compareTo(date1));
		
	}
	
	@Ignore
	@Test
	public void parseResources(){
		for(Resource ress : resourceData.getRessources()){
			System.out.println("Competence: " + ress.getCompetence().name());
			System.out.println("Start Date: " + ress.getStartDate().toString());
			System.out.println("End Date: " + ress.getEndDate().toString());
			System.out.println("Quantity: " + ress.getInitialQuantity());
			System.out.println("-------------------");
		}
	}
	
	@Test
	public void testPSGS() throws IOException{
		ResultOutputCSV.writeRessources(resourceData.getRessources(), "src/main/resources/Before_psgs/initial_availability");
		Heuristics simple = new SimpleHeuristics();
		psgs.doPSGS(simple);
		ResultOutputCSV.writeProjects(psgs.getPlanifiedTasks(), "src/main/resources/result_csv.csv");
		Task testTask = psgs.getPlanifiedTasks().get(1);
		for(Resource ress: testTask.getEffort().keySet()){
			System.out.println("Ress: " + ress.getCompetence().name());
			for(Date date : ress.getAvailability().keySet()){
				System.out.println(date.toString() + ": " + ress.getAvailability().get(date));
			}
			
		}
	}
	
	
	@Ignore
	@Test
	public void testOutputWriter() throws IOException{
		psgs.getOutput().writeIteration(3, new Date());
		//psgs.getOutput().getWriter().flush();
		ResultOutput ro = new ResultOutput("src/main/resources/test.txt");
		ro.writeIteration(3, new Date());
		//ro.closeWriter();
		
	}
	
	@Test
	public void testOutputResourceWriter() throws IOException{
		ResultOutputCSV.writeRessources(resourceData.getRessources(), "src/main/resources/After_psgs/actual_availability");
	}
	
	@Test
	public void testDivision(){
		int val = (int)(5/1.5);
		assertEquals(val, 3, 0);
	}
	
	

}
