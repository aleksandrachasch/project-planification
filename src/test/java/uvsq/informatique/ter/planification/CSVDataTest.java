package uvsq.informatique.ter.planification;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import org.apache.commons.lang3.SerializationUtils;

import uvsq.informatique.ter.planification.core.Project;
import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.data.csv.DataSource;
import uvsq.informatique.ter.planification.data.csv.ResultOutputCSV;
import uvsq.informatique.ter.planification.heuristics.CriticalRessource;
import uvsq.informatique.ter.planification.heuristics.Heuristics;
import uvsq.informatique.ter.planification.heuristics.SimpleHeuristics;
import uvsq.informatique.ter.planification.scheme.PSGS;

public class CSVDataTest {
	
	//ArrayList<Task> allTasks;
	//ArrayList<Ressource> allResources;
	DataSource source;
	SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	
	@Before
	public void initialize() throws ParseException{
		source = new DataSource("src/main/resources/data_projects.csv",
				"src/main/resources/data_ressources.csv");
		//allTasks = source.getTasks();
		//allResources = source.getRessources();
	}
	
	@Test
	public void testPSGS() throws ParseException, IOException{
		ResultOutputCSV.writeRessources(source.getRessources(), "src/main/resources/Before_psgs/available");
		PSGS psgs = new PSGS(source.getTasks(), df.parse("01.12.2016"), 
				df.parse("30.06.2017"));
		psgs.setOutput("src/main/resources/psgs_test_output.txt");
		Heuristics simple = new SimpleHeuristics();
		psgs.doPSGS(simple);
		ResultOutputCSV.writeRessources(source.getRessources(), "src/main/resources/After_psgs/remain");

		System.out.println("Size planified: " + psgs.getPlanifiedTasks().size());
		System.out.println("Size alltasks: " + psgs.getAllTasks().size());
	}

	@Ignore
	@Test
	public void testCriticalResource() throws IOException, ParseException{
		ResultOutputCSV.writeRessources(source.getRessources(), "src/main/resources/Before_psgs/available");
		PSGS psgs = new PSGS(source.getTasks(), df.parse("01.12.2016"), 
				df.parse("30.06.2017"));
		psgs.setOutput("src/main/resources/psgs_test_output.txt");
		Heuristics simple = new CriticalRessource();
		psgs.doPSGS(simple);
		ResultOutputCSV.writeRessources(source.getRessources(), "src/main/resources/After_psgs/remain");
		ResultOutputCSV.writeProjects(source.getTasks(), "src/main/resources/planified_projects.csv");
//		ResultOutputCSV.writeProjects(psgs.getPlanifiedTasks(), "src/main/resources/planified_projects.csv");
		ResultOutputCSV.writeOccupancyRate(source.getRessources(), "src/main/resources/occupancy_rate.csv");
		System.out.println("Size planified: " + psgs.getPlanifiedTasks().size());
		System.out.println("Size alltasks: " + psgs.getAllTasks().size());
		for(Resource ress : source.getRessources()){
			System.out.println("Name: " + ress.getCompetence().name());
			System.out.println("Mean: " + ress.calculateMeanAvailabilityRatePerDay() + "\n");
		}
	}
	
	@Ignore
	@Test
	public void testRandom(){
		int rnd1 = new Random().nextInt(1);
		System.out.println("Random from 1: " + rnd1);
		int rnd2 = new Random().nextInt(source.getTasks().size());
		System.out.println("Random from array: " + rnd2);
	}
	
	@Ignore
	@Test
	public void testFirstTask(){
		for(Project p : source.getProjects()){
			System.out.println("Project name: " + p.getName());
			System.out.println("Start date: " + p.getFirstTask().getInitialStart());
			System.out.println("Nb of tasks: " + p.getTasks().size()+ "\n\n");
		}
	}
	
	@Ignore
	@Test
	public void testPheromone(){
		Task t = source.getTasks().get(33);
		System.out.println(Arrays.toString(t.getPheromoneMarks()));
		double val = 50.0;
		t.setPheromoneMark(1, val);
		System.out.println(Arrays.toString(t.getPheromoneMarks()));
	}
	
	@Test
	public void testCopy() throws ParseException{
		ArrayList<Resource> copy = SerializationUtils.clone(source.getRessources());
		Date date = df.parse("01.03.2017");
		copy.get(3).getAvailability().put(date, 50.0);
		assertFalse(source.getRessources().get(3).getAvailability().get(date) == 
				copy.get(3).getAvailability().get(date));
		
		Task task = source.getTasks().get(33);
		Task copyTask = SerializationUtils.clone(task);
		task.setActualEnd(date);
		assertFalse(source.getTasks().get(33).getActualEnd() == copyTask.getActualEnd());
		
	}

}
