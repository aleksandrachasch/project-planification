package uvsq.informatique.ter.planification.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;

public class Resource implements Serializable{
	
	Competence competence;
	Date start_availability;
	Date end_availability;
	Double initial_quantity;
	Double actual_quantity;
	Map<Date,Double> availability;
	Double mean_rate_per_day;
	public Double initial_availability_per_day;
	
	public Resource(Competence comp, Date start, Date end, Double quantity){
		competence = comp;
		start_availability = start;
		end_availability = end;
		initial_quantity = quantity;
		calculateAvailability();
	}
	
	public Competence getCompetence(){
		return competence;
	}
	
	public Date getStartDate(){
		return start_availability;
	}
	
	public Date getEndDate(){
		return end_availability;
	}
	
	public Double getInitialQuantity(){
		return initial_quantity;
	}
	
	private void calculateAvailability(){
		availability = new TreeMap<Date,Double>();

		DateTime dtStart = new DateTime(start_availability);
		DateTime dtEnd = new DateTime(end_availability);
		Days duration = Days.daysBetween(dtStart.withTimeAtStartOfDay(), 
				dtEnd.withTimeAtStartOfDay());
		Double availability_per_day = initial_quantity / duration.getDays();
		initial_availability_per_day = availability_per_day;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(start_availability);
		
		while(cal.getTime().before(end_availability) || cal.getTime().equals(end_availability)){
			availability.put(cal.getTime(), availability_per_day);
			cal.add(Calendar.DATE, 1);
		}
	}
	
	public void allocateRessource(Date start, Date end, Double quantity){
		for(Date date : availability.keySet()){
			if(date.equals(start) || (date.after(start) & date.before(end))){
				availability.put(date, availability.get(date) - quantity);
			}else if(date.equals(end)){
				availability.put(date, availability.get(date) - quantity);
				break;
			}
		}
	}
	
	public void releaseRessource(Date start, Date end, Double quantity){
		/*
		 * Here I use directly Joda Time because I plan to move all the date 
		 * structure into Joda Time library
		 */
		DateTime startDate = new DateTime(start);
		DateTime endDate = new DateTime(end);
		/*
		 * as Interval object in JodaTime is end-exclusive, I add one day to the
		 * endDate so that it will count it as well
		 */
		endDate = endDate.plusDays(1);
		Interval interval = new Interval(startDate, endDate);
		for(Date date : availability.keySet()){
			DateTime currDate = new DateTime(date);
			if(interval.contains(currDate)){
				availability.put(date, availability.get(date) + quantity);
			}
		}
	}
	
	public Map<Date,Double> getAvailability(){
		return availability;
	}
	
	public double getTotalAvailability(){
		double val = 0.0;
		for(Date date : availability.keySet()){
			val += availability.get(date);
		}
		return val;
	}
	
	public double getOccupancyRate(){
		return (initial_quantity - getTotalAvailability()) / initial_quantity;
	}
	
	public double getOccupancy(Date d){
		return initial_availability_per_day - availability.get(d);
	}
	
	public boolean isAvailable(Date start, Date end, Double quantity){
		/*
		 * Convert dates into Joda Time dates in order to set up period of time when the resource should be available 
		 */
		DateTime start_date = new DateTime(start);
		DateTime end_date = new DateTime(end);
		//System.out.println("Start date of isAvailable: " + start_date.toString());
		//System.out.println("End date of isAvailable: " + end_date.toString());
		Interval interval = new Interval(start_date, end_date);
		
		Double available_quantity = 0.0;
		for(Date date : availability.keySet()){
			DateTime currDate = new DateTime(date);
			if(interval.contains(currDate) || interval.getEnd().isEqual(currDate)){
				available_quantity += availability.get(date);
			}
		}
		//System.out.println("Available quantity of " + this.competence.name() + " from " + start.toString() +
		//		" to " + end.toString() + ": " + available_quantity);
		return available_quantity >= quantity;
	}
	
	/*
	 * Calculate arithmetic mean of availability rate per day
	 */
	public double calculateMeanAvailabilityRatePerDay(){
		Collection<Double> set = availability.values();
		double val = 0.0;
		for(Double quantity : set){
			val += quantity;
		}
		mean_rate_per_day = val / set.size() ;
		return mean_rate_per_day;
	}
	
	public double calculateMeanOccupancyRatePerDay(){
		return initial_availability_per_day - calculateMeanAvailabilityRatePerDay();
	}
	/*
	 * Get dates where availability is less than the mean rate (critical days)
	 */
	public ArrayList<Date> getCriticalDays(){
		calculateMeanAvailabilityRatePerDay();
		ArrayList<Date> result = new ArrayList<Date>();
		for(Date date : availability.keySet()){
			if(availability.get(date) <= mean_rate_per_day){
				result.add(date);
			}
		}
		return result;
	}
	/*
	 * Get dates where demand on the resource is low (more than the mean rate of availability)
	 */
	public ArrayList<Date> getNonCriticalDays(){
		calculateMeanAvailabilityRatePerDay();
		ArrayList<Date> result = new ArrayList<Date>();
		for(Date date : availability.keySet()){
			if(availability.get(date) > mean_rate_per_day){
				result.add(date);
			}
		}
		return result;
	}
}
