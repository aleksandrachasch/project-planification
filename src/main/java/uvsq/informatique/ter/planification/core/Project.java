package uvsq.informatique.ter.planification.core;

import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

public class Project implements Serializable{
	
	ArrayList<Task> activities = new ArrayList<Task>();
	int priority;
	String name;
	boolean delayed = false;
	
	public Project(String name, int priority){
		this.name = name;
		this.priority = priority;
	}
	
	public int getPriority(){
		return priority;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean isDelayed(){
		return delayed;
	}
	
	public void addTask(Task task){
		activities.add(task);
	}
	
	public ArrayList<Task> getTasks(){
		return activities;
	}
	
	public void delayDates(Date dateOfDelay){
		DateTime dtStart = new DateTime(getFirstTask().getInitialStart());
		DateTime dtEnd = new DateTime(dateOfDelay);
		Duration duration = new Duration(dtStart, dtEnd);
		int numberOfDays = (int) duration.getStandardDays();
		
		for(Task task : activities){
			task.delayDates(numberOfDays);
		}
		delayed = true;
	}
	
	public Task getFirstTask(){
		Date minDate = null;
		Task result = null;
		for(Task task : activities){
			if(minDate == null || minDate.compareTo(task.getInitialStart()) > 0){
				minDate = task.getInitialStart();
				result = task;
			}
		}
		return result;
	}
	
	public boolean isEligibleToStart(Date currentDate){
		return getFirstTask().isEligibleToStart(currentDate);
	}
	
	/*
	 * TODO: make tasks of priority projects be able to compress the duration (currently each task of priority project starts exactly at it's initial
	 * start date)
	 */
	public void allocateProject(){
		for(Task task : activities){
			task.allocateTask(task.getInitialStart(), null);
		}
	}

}
