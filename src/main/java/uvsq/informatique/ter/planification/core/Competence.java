package uvsq.informatique.ter.planification.core;

import java.io.Serializable;

public enum Competence implements Serializable{
	
	Exploit, Testeur_IT, CP_IT, CP_Metier, Architecte, Tech_Lead, Dev,
	Recetteur_Fonctionnel, Resp_Domaine, Manager, Resp_Application, CP_BI;
	
	public static Competence getCompetence(String compName){
		if(compName.equals("Exploit")){
			return Exploit;
		}else if(compName.equals("Testeur_IT")){
			return Testeur_IT;
		}else if(compName.equals("CP_IT")){
			return CP_IT;
		}else if(compName.equals("CP_Metier")){
			return CP_Metier;
		}else if(compName.equals("Architecte")){
			return Architecte;
		}else if(compName.equals("Tech_Lead")){
			return Tech_Lead;
		}else if(compName.equals("Dev")){
			return Dev;
		}else if(compName.equals("Recetteur_Fonctionnel")){
			return Recetteur_Fonctionnel;
		}else if(compName.equals("Resp_Domaine")){
			return Resp_Domaine;
		}else if(compName.equals("Manager")){
			return Manager;
		}else if(compName.equals("Resp_Application")){
			return Resp_Application;
		}else if(compName.equals("CP_BI")){
			return CP_BI;
		}else{
			throw new IllegalArgumentException("Invalid competence name");
		}
				
	}

}
