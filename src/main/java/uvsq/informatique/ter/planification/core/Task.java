package uvsq.informatique.ter.planification.core;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Days;

import uvsq.informatique.ter.planification.optimization.Decision;
import uvsq.informatique.ter.planification.optimization.HeuristicsMark;

public class Task implements Serializable{
	
	private Project project;
	private int id;
	private String name;
	private Date input_start; //used for delayed projects
	private Date input_end;  //used for delayed projects
	private Date initial_start;
	private Date initial_end;
	private Date actual_start;
	private Date actual_end;
	private Map<Resource,Double> effort = new HashMap<Resource, Double>();
	private Map<Resource,Double> actualEffortPerDay = new HashMap<Resource, Double>();
	
	double[] heuristicsMarks;
	double[] pheromoneMarks = new double[3];
	
	public Task(int id, String name, Project project, Date initial_start, Date initial_end){
		this.id = id;
		this.name = name;
		this.project = project;
		this.initial_start = this.input_start = initial_start;
		this.initial_end = this.input_end = initial_end;
		double val = 1.0/3;
		for(int i = 0; i < 3; i++){
			pheromoneMarks[i] = val;
		}
	}
	
	public Date getInitialStart(){
		return initial_start;
	}
	
	public Date getInitialEnd(){
		return initial_end;
	}
	
	public Date getActualStart(){
		return actual_start;
	}
	
	public Date getActualEnd(){
		return actual_end;
	}
	/*
	 * Two following methods are used when we need the very initial start date after
	 * it has been modified during delay
	 */
	public Date getInputStart(){ 
		return input_start;
	}
	
	public Date getInputEnd(){
		return input_end;
	}
	
	public Project getProject(){
		return project;
	}
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public void addEffort(Resource ress, Double quantity){
		if(effort.keySet().contains(ress)){
			effort.put(ress, effort.get(ress) + quantity);
		}else{
			effort.put(ress, quantity);
		}
	}
	
	public int getInitialDuration(){
		/*
		 * TODO: probably better to migrate all code related to Dates to Joda-Time library
		 */
		DateTime dtStart = new DateTime(initial_start);
		DateTime dtEnd = new DateTime(initial_end);
		Days duration = Days.daysBetween(dtStart.withTimeAtStartOfDay(), 
				dtEnd.withTimeAtStartOfDay());
		return duration.getDays();
	}
	
	public Double getEffortbyRessource(Resource ress){
		return effort.get(ress);
	}
	
	public Map<Resource,Double> getEffort(){
		return effort;
	}
	
	public Set<Resource> getNeededCompetence(){
		return effort.keySet();
	}
	
	public void setActualStart(Date start){
		actual_start = start;
	}
	
	public void setActualEnd(Date end){
		actual_end = end;
	}
	
	public boolean needsRessource(Resource ress){
		return effort.keySet().contains(ress);
	}
	
	public void delayDates(int numberOfDays){
		Calendar cal = Calendar.getInstance();
		cal.setTime(initial_start);
		cal.add(Calendar.DATE, numberOfDays);
		initial_start = cal.getTime();
		
		cal.setTime(initial_end);
		cal.add(Calendar.DATE, numberOfDays);
		initial_end = cal.getTime();
	}
	 /*
	  * Return minimal duration of the task (initial duration / 1.5
	  */
	private int getMinimalDuration(){
		return (int) (getInitialDuration() / 1.5);
	}
	public int getActualDuration(Date startDate, Date endDate){
		DateTime start = new DateTime(startDate);
		DateTime end;
		if(endDate == null){
			end = new DateTime(initial_end);
		}else{
			end = new DateTime(endDate);
		}
		
		Days actualDuration = Days.daysBetween(start, end);
		return actualDuration.getDays();
	}
	
	public Double getTotalEffort(){
		Double result = 0.0;
		for(Resource ress : effort.keySet()){
			result += effort.get(ress);
		}
		return result;
	}
	
	public boolean isEligibleToStart(Date start){
		if(start.equals(initial_start) || start.after(initial_start)){
			return true;
		}else{
			return false;
		}
	}
	
	public Date recalculateEndDate(Date new_start_date, int duration){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new_start_date);
		cal.add(Calendar.DATE, duration);
		return cal.getTime();
	}
	
	public Date recalculateStartDate(Date new_end_date, int duration){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new_end_date);
		cal.add(Calendar.DATE, -duration);
		return cal.getTime();
	}
	
	private double getActualEffortPerDay(Resource ress, int actualDuration){
		return effort.get(ress) / actualDuration;
		
	}
	
	public void applyDecision(Decision dec){
		switch(dec){
		case CONTRACT_AND_DELAY_START :
			changeActualDates(recalculateStartDate(initial_end, getMinimalDuration()), initial_end);
			break;
		case CONTRACT_AND_FORWARD_END :
			changeActualDates(initial_start, recalculateEndDate(initial_start, getMinimalDuration()));
			break;
		case DO_NOT_CONTRACT:
			changeActualDates(initial_start, initial_end);
			break;
		}
	}
	
	public double getActualEffortPerDay(Resource ress){
		return actualEffortPerDay.get(ress);
	}
	
	public void allocateTask(Date actualStartDate, Date actualEndDate){
		int newDuration = getActualDuration(actualStartDate, actualEndDate);
		
		actual_start = actualStartDate;
		if(actualEndDate == null){
			actual_end = initial_end;
		}else{
			actual_end = actualEndDate;
		}
		
		//actual_end = recalculateEndDate(actualStartDate, newDuration);
		
		for(Resource ress : effort.keySet()){
			ress.allocateRessource(actual_start, actual_end, getActualEffortPerDay(ress, newDuration));
			actualEffortPerDay.put(ress, getActualEffortPerDay(ress, newDuration));
		}
	}
	
	public void changeActualDates(Date newStartDate, Date newEndDate){
		for(Resource ress : effort.keySet()){
			ress.releaseRessource(actual_start, actual_end, getActualEffortPerDay(ress));
		}
		actual_start = newStartDate;
		actual_end = newEndDate;
		allocateTask(newStartDate, newEndDate);
		
	}
	
	public void restoreInitialDates(){
		initial_start = input_start;
		initial_end = input_end;
	}
	
	public void setHeuristicsMarks(Resource ress){
		HeuristicsMark heuristics = new HeuristicsMark(this, ress.getCriticalDays());
		this.heuristicsMarks = heuristics.getMarks();
	}
	
	public double[] getHeuristicsMarks(){
		return heuristicsMarks;
	}
	
	public void setPheromoneMark(int position, double mark){
		pheromoneMarks[position] = mark;
	}
	
	public double[] getPheromoneMarks(){
		return pheromoneMarks;
	}
	
	public void updatePheromoneMarks(Decision dec, double evaporationRate, double BFS){
		int index = Decision.getIndexOfDecision(dec);
		pheromoneMarks[index] = pheromoneMarks[index] + evaporationRate*BFS;
	}
	
	public void evaporatePheromone(double evaporationRate){
		for(int i = 0; i < pheromoneMarks.length; i++){
			pheromoneMarks[i] = (1 - evaporationRate) * pheromoneMarks[i];
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/*
	 * TODO: probably not a good idea to compare objects only by its ID (incorporate
	 * other attributes as well)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
