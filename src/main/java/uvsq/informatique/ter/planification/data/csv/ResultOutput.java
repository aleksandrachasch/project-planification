package uvsq.informatique.ter.planification.data.csv;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import uvsq.informatique.ter.planification.core.Project;
import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.optimization.Decision;

public class ResultOutput {
	
	String outputfilename;
	Writer writer;
	SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	
	public ResultOutput(String outputfilename){
		this.outputfilename = outputfilename;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(outputfilename));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Writer getWriter(){
		return writer;
	}
	
	public void writeIteration(int iteration, Date currentDate) throws IOException{
		writer.write("------------Iteration № " + iteration + "------------\n");
		writer.write("----Current Date: " + currentDate.toString() + "-------\n");
		writer.flush();
	}
	
	public void writePriorityProjects(Set<Project> projects){
		try{
			writer.write("------------Priority projects----------\n");
			writer.flush();
			for(Project p : projects){
				writer.write(p.getName() + "\n");
				writer.flush();
			}
			writer.write("------------End priority projects-------\n");
			writer.flush();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public void writePlanifiedProjects(ArrayList<Task> planified){
		try {
			writer.write("----------Planified tasks before eligible----------\n");
			writer.flush();
			for(Task t : planified){
				writer.write("Project: " + t.getProject().getName() + "\n");
				writer.write("Name: " + t.getName() + "\n");
				writer.write("Initial start: " + t.getInitialStart().toString() + "\n");
				writer.write("Initial end: " + t.getInitialEnd().toString() + "\n");
				writer.write("Actual start: " + t.getActualStart().toString() + "\n");
				writer.write("Initial end: " + t.getActualEnd() + "\n");
				writer.write("##################\n");
				writer.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void writeDelayedProject(Project p, Date currentDate) throws IOException{
		writer.write("\n\n------------Delayed project at " + currentDate.toString() 
				+ "--------------------\n");
		writer.write("Project name: " + p.getName() + "\n");
		writer.write("Initial start: " + p.getFirstTask().getInitialStart() + "\n");
		writer.write("----------End delayed project---------------\n\n");
	}
	
	public void writeEligible(ArrayList<Task> eligible, Date currentDate, String recalculated){
		try {
			writer.write("\n\n-------------Eligible tasks at " + currentDate.toString() + "----------\n");
			writer.write("----------" + recalculated + "--------------\n");
			writer.flush();
			for(Task t : eligible){
				writer.write("Project: " + t.getProject().getName() + "\n");
				writer.write("Name: " + t.getName() + "\n");
				writer.write("Initial start: " + t.getInitialStart().toString() + "\n");
				writer.write("Initial end: " + t.getInitialEnd().toString() + "\n");
				writer.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeRessources(ArrayList<Resource> allResources) throws IOException{
		writer.write("------------Ressources available---------\n");
		writer.flush();
		for(Resource ress : allResources){
			writer.write(ress.getCompetence().name() + "\n");
			writer.flush();
			for(Date d : ress.getAvailability().keySet()){
				writer.write(d + ": " + ress.getAvailability().get(d) + "\n");
				writer.flush();
			}
		}
	}
	
	public void writeBestFoundSolution(Map<Task, Decision> solution, double meanRate) throws IOException{
		writer.write("\n\n----------Best found solution-----------\n");
		writer.write("Mean rate: " + meanRate + "%\n");
		for(Task task : solution.keySet()){
			writer.write("Task name: " + task.getName() + "\n");
			writer.write("Project name: " + task.getProject().getName() + "\n");
			writer.write("Decision: " + solution.get(task).name() + "\n");
			writer.write("--------------------------\n");
		}
		writer.flush();
	}
	
	public void writeHeuristicMarks(ArrayList<Task> tasks) throws IOException{
		writer.write("\n\n------------Heuristics marks------------\n");
		for(Task task : tasks){
			writer.write("Task name: " + task.getName() + "\n");
			writer.write("Project name: " + task.getProject().getName() + "\n");
			writer.write("Heuristic marks: " + Arrays.toString(task.getHeuristicsMarks()) + "\n");
			writer.write("---------------------\n");
		}
	}
	
	public void writeCriticalDays(ArrayList<Date> dates) throws IOException{
		writer.write("\n\n-----------Critical dates----------");
		for(Date date : dates){
			writer.write(df.format(date) + "\n");
		}
		writer.write("-------End critical dates---------\n\n");
	}
	
	public void closeWriter(){
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
