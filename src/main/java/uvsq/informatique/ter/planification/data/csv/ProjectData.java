package uvsq.informatique.ter.planification.data.csv;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uvsq.informatique.ter.planification.core.Competence;
import uvsq.informatique.ter.planification.core.Project;
import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;

public class ProjectData {
	
	private String filename;
	private ArrayList<Task> alltasks = new ArrayList<Task>();
	public ArrayList<Project> allprojects = new ArrayList<Project>();
	private ArrayList<String[]> linesFromFile = new ArrayList<String[]>();
	private ArrayList<Resource> allRessources;
	
	private final SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	
	public ProjectData(String filename, ArrayList<Resource> allRessources) throws ParseException{
		this.allRessources = allRessources;
		this.filename = filename;
		this.linesFromFile = readLinesFromFile();
		readTasks();
	}
	
	public ArrayList<Task> getAllTasks(){
		return alltasks;
	}
	
	public ArrayList<Resource> getAllRessources(){
		return allRessources;
	}
	
	public ArrayList<Project> getAllProjects(){
		return allprojects;
	}
	
	public void readTasks() throws ParseException{
		readProjects();
		int n = 0;
		boolean taskExists;
		for(String[] line : linesFromFile) {
			String taskName = line[2];
			Project p = getProject(line[0]);
			Double effort = Double.valueOf(line[5]);
			Date startDate = df.parse(line[3]);
			Date finishDate = df.parse(line[4]);
			taskExists = false;
			for(Task t : alltasks){
				if(t.getName().equals(taskName) && t.getProject() == p){
						//&& t.getInitialStart().equals(startDate) 
						//&& t.getInitialEnd().equals(finishDate)){ //task already created => need to add new resources
					//System.out.println(line[1]);
					t.addEffort(getRessourceByCompetence(line[1]), effort);
					taskExists = true;
				}
			}
			if(!taskExists){
				//Date startDate = df.parse(line[3]);
				//Date finishDate = df.parse(line[4]);
				Task task = new Task(n, taskName, p, startDate, finishDate);
				task.addEffort(getRessourceByCompetence(line[1]), effort);
				p.addTask(task);
				alltasks.add(task);
				n += 1;
				}
			}
	}
	
	private Resource getRessourceByCompetence(String compName){
		for(Resource ress : allRessources){
			if(ress.getCompetence().name().equals(compName)){
				return ress;
			}
		}
		throw new IllegalArgumentException("Such competence does not exist!");
	}
	
	private void readProjects(){
		Map<String,Integer> projectNames = new HashMap<String,Integer>();
		for(String[] line : linesFromFile){
			projectNames.put(line[0], Integer.parseInt(line[line.length-1]));
		}
		for(String projectName : projectNames.keySet()){
			Project project = new Project(projectName, projectNames.get(projectName));
			allprojects.add(project);
		}
	}
	
	private Project getProject(String projectName){
		for(Project p : allprojects){
			if(p.getName().equals(projectName)){
				return p;
			}
		}
		throw new IllegalArgumentException("Project with such name does not exist!");
	}
	
	
	private ArrayList<String[]> readLinesFromFile(){
		ArrayList<String[]> res = new ArrayList<String[]>();
		BufferedReader br = null;
		String line = "";
		try{
			br = new BufferedReader(new FileReader(filename));
			while((line = br.readLine()) != null){
				String[] task_attr = line.split(";");
				res.add(task_attr);
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

}
