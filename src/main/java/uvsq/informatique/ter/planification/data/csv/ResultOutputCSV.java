package uvsq.informatique.ter.planification.data.csv;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;

public class ResultOutputCSV {
	
	private static SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	
	public static void writeProjects(ArrayList<Task> planified, String filename) throws IOException{
		Writer writer  = new OutputStreamWriter(new FileOutputStream(filename));
		
		writer.write("ACT;ROLE;ACL;INITIAL_START_DATE;INITIAL_END_DATE;START_DATE;END_DATE;PRIORITY;DELAYED;ID\n");
		for(Task task : planified){
			for(Resource ress : task.getEffort().keySet()){
				writer.write(task.getProject().getName() + ";");
				writer.write(ress.getCompetence().name() + ";");
				writer.write(task.getName() + ";");
				writer.write(df.format(task.getInitialStart()) + ";");
				writer.write(df.format(task.getInitialEnd()) + ";");
				if(task.getActualStart()!= null){
					writer.write(df.format(task.getActualStart()) + ";");
					writer.write(df.format(task.getActualEnd()) + ";");
				}else{
					writer.write(";;");
				}
				writer.write(task.getProject().getPriority() + ";");
				writer.write(task.getProject().isDelayed() + ";");
				writer.write(task.getId() + "\n");
			}
		}
		writer.close();
	}
	
	public static void writeRessources(ArrayList<Resource> resources, String filename) throws IOException{
		for(Resource ress : resources){
			Writer writer = new OutputStreamWriter(new FileOutputStream(filename +"_" + ress.getCompetence().name() + ".csv"));
			writer.write("DATE;AVAILABLE_EFFORT\n");
			for(Date d : ress.getAvailability().keySet()){
				writer.write(df.format(d) + ";" + ress.getOccupancy(d) + "\n");
			}
			writer.close();
		}
		
	}
	
	public static void writeInitialAvailability(ArrayList<Resource> resources, String filename) throws IOException{
		for(Resource ress : resources){
			Writer writer = new OutputStreamWriter(new FileOutputStream(filename +"_" + ress.getCompetence().name() + ".csv"));
			writer.write("DATE;AVAILABLE_EFFORT\n");
			for(Date d : ress.getAvailability().keySet()){
				writer.write(df.format(d) + ";" + ress.getAvailability().get(d) + "\n");
			}
			writer.close();
		}
	}
	
	public static void writeOccupancyRate(ArrayList<Resource> resources, String filename) throws IOException{
		Writer writer = new OutputStreamWriter(new FileOutputStream(filename));
		writer.write("RESOURCE;OCCUPANCY_RATE\n");
		for(Resource ress : resources){
			writer.write(ress.getCompetence().name() + ";" + ress.getOccupancyRate()*100 + "%\n");
		}
		writer.close();
	}

}
