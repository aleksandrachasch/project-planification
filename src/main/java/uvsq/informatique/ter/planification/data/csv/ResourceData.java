package uvsq.informatique.ter.planification.data.csv;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import uvsq.informatique.ter.planification.core.Competence;
import uvsq.informatique.ter.planification.core.Resource;

public class ResourceData {
	
	private String filename;
	
	private final SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	private ArrayList<String[]> linesFromFile = new ArrayList<String[]>();
	private ArrayList<Resource> allRessources = new ArrayList<Resource>();
	
	public ResourceData(String filename) throws ParseException{
		this.filename = filename;
		this.linesFromFile = readLinesFromFile();
		readRessourceData();
	}
	
	public ArrayList<Resource> getRessources(){
		return allRessources;
	}
	
	private void readRessourceData() throws ParseException{
		//read headers
		String[] headers = linesFromFile.get(0);
		Date startDate = df.parse(headers[1]);
		Date endDate = df.parse(headers[headers.length-1]);
		for(int x = 1; x < linesFromFile.size(); x++){
			String[] line = linesFromFile.get(x);
			Competence comp = Competence.getCompetence(line[0]);
			Double quantity = 0.0;
			for(int i = 1; i < line.length; i++){
				Double val = Double.valueOf(line[i]);
				quantity += val;
			}
			Resource ress = new Resource(comp, startDate, endDate, quantity);
			allRessources.add(ress);
		}
	}
	
	private ArrayList<String[]> readLinesFromFile(){
		ArrayList<String[]> res = new ArrayList<String[]>();
		BufferedReader br = null;
		String line = "";
		try{
			br = new BufferedReader(new FileReader(filename));
			while((line = br.readLine()) != null){
				String[] ress_attr = line.split(";");
				res.add(ress_attr);
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}
	
	

}
