package uvsq.informatique.ter.planification.data.csv;
import java.text.ParseException;
import java.util.ArrayList;

import uvsq.informatique.ter.planification.core.Project;
import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;

public class DataSource {
	
	private final ArrayList<Task> allTasks;
	private final ArrayList<Resource> allRessources;
	
	private final ArrayList<Project> allprojects;
	
	public DataSource(String tasksFilename, String resourcesFilename) throws ParseException{
		ResourceData rd = new ResourceData(resourcesFilename);
		allRessources = rd.getRessources();
		ProjectData pd = new ProjectData(tasksFilename, allRessources);
		allTasks = pd.getAllTasks();
		allprojects = pd.getAllProjects();
	}
	
	public ArrayList<Task> getTasks(){
		return allTasks;
	}
	
	public ArrayList<Resource> getRessources(){
		return allRessources;
	}
	
	public ArrayList<Project> getProjects(){
		return allprojects;
	}

}
