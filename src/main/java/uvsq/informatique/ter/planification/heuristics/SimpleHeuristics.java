package uvsq.informatique.ter.planification.heuristics;

import java.util.ArrayList;

import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.scheme.PSGS;

public class SimpleHeuristics implements Heuristics{
	
	public int getIndexTaskToSchedule(PSGS psgs){
		int n = 0;
		Double maxEffort = 0.0;
		ArrayList<Task> eligible = psgs.getEligibleSet();
		for(Task task : eligible){
			if(task.getTotalEffort() > maxEffort){
				maxEffort = task.getTotalEffort();
				n = eligible.indexOf(task);
			}
		}
		return n;
	}
}
