package uvsq.informatique.ter.planification.heuristics;

import java.util.ArrayList;

import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.scheme.PSGS;

public interface Heuristics {
	
	int getIndexTaskToSchedule(PSGS psgs);

}
