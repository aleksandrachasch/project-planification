package uvsq.informatique.ter.planification.heuristics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.scheme.PSGS;

public class CriticalRessource implements Heuristics{
	
	

	public int getIndexTaskToSchedule(PSGS psgs) {
		
		Resource criticalRess = psgs.getCriticalRessource();
		Task nextTask = null;
		Double maxNeed = null;
		for(Task task : psgs.getEligibleSet()) {
			if(task.needsRessource(criticalRess)){
				if((maxNeed == null && nextTask == null) || task.getEffortbyRessource(criticalRess) > maxNeed) {
					nextTask = task;
					maxNeed = task.getEffortbyRessource(criticalRess);
				}
			}

		}
		/*
		 * If there are no more tasks which need critical resource
		 * return a random task
		 */
		if(maxNeed == null && nextTask == null){
			int rnd = new Random().nextInt(psgs.getEligibleSet().size());
			return rnd;
		}
		return psgs.getEligibleSet().indexOf(nextTask);
	}

}
