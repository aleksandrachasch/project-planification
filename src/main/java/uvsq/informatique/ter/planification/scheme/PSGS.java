package uvsq.informatique.ter.planification.scheme;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import uvsq.informatique.ter.planification.core.Project;
import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.data.csv.ResultOutput;
import uvsq.informatique.ter.planification.data.csv.ResultOutputCSV;
import uvsq.informatique.ter.planification.heuristics.Heuristics;

public class PSGS {
	
	private ArrayList<Task> alltasks;
	private Date period_start;
	private Date period_end;
	
	private Date current_date;
	private ArrayList<Task> planned = new ArrayList<Task>();
	private Set<Project> priorityProjects = new HashSet<Project>();
	private ArrayList<Task> eligible = new ArrayList<Task>();
	private Set<Project> currentProjects = new HashSet<Project>();
	volatile ResultOutput output;
	
	public PSGS(ArrayList<Task> alltasks, Date period_start, Date period_end){
		this.alltasks = alltasks;
		this.period_start = period_start;
		this.period_end = period_end;
		current_date = period_start;
		setUpPriorityProjects();
	}
	
	public void setOutput(String filename) throws IOException{
		output = new ResultOutput(filename);
		output.getWriter().write("Test string");
	}
	
	public ResultOutput getOutput(){
		return output;
	}
	
	public ArrayList<Task> getEligibleSet(){
		return eligible;
	}
	
	public ArrayList<Task> getAllTasks(){
		return alltasks;
	}
	
	public ArrayList<Task> getPlanifiedTasks(){
		return planned;
	}
	
	public void doPSGS(Heuristics heuristics) throws IOException{
		
		output.writePriorityProjects(priorityProjects);
		planifyPriorityProjects();
		output.writePlanifiedProjects(planned);
		int n = 0;
		//while(alltasks.size() > planified.size()){
		while(current_date.before(period_end)){
			//System.out.println("Iteration #: " + n);
			//if(n == 52){
				//break;
			//}
			//System.out.println("Current date: " + current_date.toString());
			output.writeIteration(n, current_date); //log
			output.writePriorityProjects(priorityProjects); //log
			
			planifyPriority();
			
			//output.writePlanifiedProjects(planified); //log
			
			eligible = calculateEligible(alltasks);
			
			output.writeEligible(eligible, current_date, "initial"); //log
			
			while(eligible.size() != 0){
				Task currTask = eligible
						.get(heuristics.getIndexTaskToSchedule(this));
				currTask.allocateTask(current_date, null);
				planned.add(currTask);
				currentProjects.add(currTask.getProject());
				eligible.remove(currTask);
				eligible = calculateEligible(eligible);
				output.writeEligible(eligible, current_date, "recalculated");
			}
			
			updateCurrentDate();
			n += 1;
		}
		output.closeWriter(); //log
	}	
	
	private ArrayList<Task> calculateEligible(ArrayList<Task> tasks){
		ArrayList<Task> result = new ArrayList<Task>();
		for(Task task : tasks){
			//System.out.println("Current task: " + task.getName());
			if(!planned.contains(task)){
				if(task.isEligibleToStart(current_date) && 
						hasEnoughRessources(task, current_date)){
					result.add(task);
				}
			}
		}
		return result;
	}
	
	private void setUpPriorityProjects(){
		for(Task task : alltasks){
			if(task.getProject().getPriority() == 1){
				priorityProjects.add(task.getProject());
			}
		}
	}
	
	/*
	 * Used to allocate projects of priority 1
	 */
	private void planifyPriorityProjects(){
		Iterator<Project> iterator = priorityProjects.iterator();
		while(iterator.hasNext()){
			Project element = iterator.next();
			//if(element.isEligibleToStart(current_date)){
				element.allocateProject();
				for(Task task : element.getTasks()){
					planned.add(task);
				}
				iterator.remove();
			//}
		}
	}
	
	/*
	 * Used to allocate tasks which:
	 * 1) Has been compressed for it's maximum (1.5 / initial duration)
	 * 2) Its projects has already been allocated
	 */
	private void planifyPriority() throws IOException{
		for(Task task : alltasks){
			 /* This value is used to check if task duration is equal to
			 * 1.5 * initial_duration (i.e., it's minimal
			 * possible duration). If this is the case, we need to allocate 
			 * this task as soon as possible
			 */
			int durationFactor = (int)(task.getInitialDuration() / 1.5);
			
			/*
			 * If task belongs to priority project - allocate it.
			 */
			
			/*
			 * TODO: What is there is not enough resources to allocate a task
			 * of a priority project? Delay already allocated project with less
			 * priority
			 */
//			if(priorityProjects.contains(task.getProject()) && !planified.contains(task)){
//				if(task.isEligibleToStart(current_date)){
//					task.allocateTask(current_date);
//					planified.add(task);
//				}
	
			/*
			 * If a task has been already delayed for Factor = 1.5N
			 */
			if(task.isEligibleToStart(current_date)){
				if(task.getActualDuration(current_date, null) == durationFactor && !planned.contains(task)){
					if(hasEnoughRessources(task, current_date)){
						task.allocateTask(current_date, null);
						planned.add(task);
					}else{
						delayProject(task.getProject());
					}

				}
			/*
			 * If project of the task has been already started - allocate it.
			 * If there is not enough resources - delay the project
			 */
			}else if(currentProjects.contains(task.getProject()) && !planned.contains(task)){
				if(task.isEligibleToStart(current_date)){
					if(hasEnoughRessources(task, current_date)){
						task.allocateTask(current_date, null);
						planned.add(task);
					}//else{
						//delayProject(task.getProject());
					//}
				}
			}
		}
		/*
		 * Here we do not check if there is enough resources available, because
		 * these are either priority projects or tasks from projects which have 
		 * already been started. So if we find out that there is not enough
		 * resources for a priority task - we need to delay the project (besides
		 * the task of priority 1)
		 */
	}
	private void delayProject(Project project) throws IOException {
		output.writeDelayedProject(project, current_date);
		/*
		 * Delay the dates of project so that it's start date would be current dat
		 * TODO: make more sophisticated way to delay projects
		 */
		for(Task task : project.getTasks()){
			if(planned.contains(task)){
				releaseRessources(task);  //should always first release
				task.setActualStart(null); //then set actual dates to null
				task.setActualEnd(null);
				planned.remove(task);
			}
		}
		/*
		 * Modify start dates and end dates of each task in project
		 */
		
		project.delayDates(current_date);	
	}

	private void releaseRessources(Task task) {
		for(Resource ress : task.getEffort().keySet()){
			ress.releaseRessource(task.getActualStart(), task.getActualEnd(),
					task.getActualEffortPerDay(ress));
		}
		
	}

	private boolean hasEnoughRessources(Task task, Date startDate){
		//System.out.println("#######################################");
		//System.out.println("Task checked for resource availability:");
		//System.out.println("Project: " + task.getProject().getName());
		//System.out.println("Name: " + task.getName());
		//System.out.println("Initial start date: " + task.getInitialStart().toString());
		//System.out.println("Initial end date: " + task.getInitialEnd().toString());
		for(Resource ress : task.getEffort().keySet()){
			if(!ress.isAvailable(startDate, task.getInitialEnd(), 
					task.getEffort().get(ress))){
				return false;
			}
		}
		//System.out.println("#######################################");
		return true;
		
	}
	
//	private void updateCurrentDate(){
//		Date minEndDate = null;
//		/*
//		 * If there were eligible tasks for the current dates, the earliest finish
//		 * date of a planified task is chosen, otherwise we search for the earliest 
//		 * start task in not-planified projects
//		 */
//		if(!planified.isEmpty()){
//			for(Task task : planified){
//				if(minEndDate == null){
//					if(task.getActualEnd().after(current_date)){
//						minEndDate = task.getActualEnd();
//					}
//				}else{
//					if(task.getActualEnd().after(current_date) && task.getActualEnd().before(minEndDate)){
//						minEndDate = task.getActualEnd();
//					}			
//				}
//			}
//		}else{
//			for(Task task : alltasks){
//				if(minEndDate == null){
//					minEndDate = task.getInitialStart();
//				}else{
//					if(task.getInitialStart().before(minEndDate)){
//						minEndDate = task.getInitialStart();
//					}
//				}
//			}
//		}
//
//		current_date = minEndDate;
//	}
	
	private void updateCurrentDate(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(current_date);
		cal.add(Calendar.DATE, 1);
		current_date = cal.getTime();
	}
	
	
	private Map<Resource, Double> getTotalNeed(){
		Map<Resource, Double> totalNeed = new HashMap<Resource, Double>();
		
		for(Task task : alltasks) {
			for(Resource ress : task.getEffort().keySet()) {
				if(totalNeed.containsKey(ress)) {
					totalNeed.put(ress, totalNeed.get(ress) + task.getEffortbyRessource(ress));
				} else {
					totalNeed.put(ress, task.getEffortbyRessource(ress));
				}
			}
		}
		return totalNeed;
	}
	
	public Resource getCriticalRessource() {
		Map<Resource, Double> totalNeed = getTotalNeed();
		Entry<Resource,Double> maxNeed = null;
		for(Entry<Resource,Double> entry : totalNeed.entrySet()) {
			if(maxNeed == null || maxNeed.getValue() < entry.getValue()) {
				maxNeed = entry;
			}
		}
		return maxNeed.getKey();
	}

}
