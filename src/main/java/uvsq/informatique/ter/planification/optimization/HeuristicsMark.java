package uvsq.informatique.ter.planification.optimization;

import java.util.ArrayList;
import java.util.Date;

import uvsq.informatique.ter.planification.core.Task;

public class HeuristicsMark {
	
	/*
	 * For each task we calculate 3 marks:
	 * Mark 1: Contract duration and move start date
	 * Mark 2: Contract duration and move end date
	 * Mark 3: Do not contract duration
	 * 
	 * We give mark = 2 if the solution is good and mark = 1 if the solution is bad
	 */
	double [] marks = new double[3];
	private final Task task;
	private Date optimalStartDate;
	private Date optimalEndDate;
	private final ArrayList<Date> criticalDates;
	
	public HeuristicsMark(Task task, ArrayList<Date> criticalDates){
		this.task = task;
		this.criticalDates = criticalDates;
		System.out.println("Calculating heuristics for task: " + task.getName());
		System.out.println("Project: " + task.getProject().getName());
		calculateMarkOne();
		calculateMarkTwo();
		calculateMarkThree();
	}
	
	private void calculateMarkOne(){
		if(criticalDates.contains(task.getActualStart())){
			marks[0] = 2;
		}else{
			marks[0] = 1;
		}
	}
	
	private void calculateMarkTwo(){
		if(criticalDates.contains(task.getActualStart())){
			marks[1] = 2;
		}else{
			marks[1] = 1;
		}
	}
	
	private void calculateMarkThree(){
		if(criticalDates.contains(task.getActualStart()) && criticalDates.contains(task.getActualEnd())){
			marks[2] = 2;
		}else if(!criticalDates.contains(task.getActualStart()) &
				!criticalDates.contains(task.getActualEnd())){
			marks[2] = 2;
		}else{
			marks[2] = 1;
		}
	}
	
	public double[] getMarks(){
		return marks;
	}
	
	

}
