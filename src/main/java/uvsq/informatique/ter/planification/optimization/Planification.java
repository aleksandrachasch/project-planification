package uvsq.informatique.ter.planification.optimization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.data.csv.ResultOutput;
import uvsq.informatique.ter.planification.heuristics.Heuristics;
import uvsq.informatique.ter.planification.heuristics.SimpleHeuristics;
import uvsq.informatique.ter.planification.scheme.PSGS;

public class Planification {
	
	private final ArrayList<Task> allTasks;
	private final Date period_start;
	private final Date period_end;
	private ArrayList<Task> delayedTasks = new ArrayList<Task>();
	private PSGS psgs;
	private ResultOutput output;
	private Heuristics heuristics = new SimpleHeuristics();
	
	public Planification(ArrayList<Task> allTasks, Date period_start, Date period_end){
		this.allTasks = allTasks;
		this.period_start = period_start;
		this.period_end = period_end;
		setDelayedTasks();
	}
	
	private void setDelayedTasks(){
		for(Task task : allTasks){
			if(task.getProject().isDelayed()){
				task.restoreInitialDates();//return very initial dates of the task (given as input)
				delayedTasks.add(task);
			}
		}
	}
	
	public void setOutput(String filename){
		output = new ResultOutput(filename);
	}
	
	public void setHeuristics(Heuristics h){
		heuristics = h;
	}
	
	public PSGS getPSGS(){
		return psgs;
	}
	
	public void reintegrateDelayedProjects(String outputFilename) throws IOException{
		psgs = new PSGS(delayedTasks, period_start, period_end);
		psgs.setOutput(outputFilename);
		psgs.doPSGS(heuristics);
	}

}
