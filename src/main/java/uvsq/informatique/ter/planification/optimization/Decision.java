package uvsq.informatique.ter.planification.optimization;

public enum Decision {
	
	CONTRACT_AND_DELAY_START, CONTRACT_AND_FORWARD_END, DO_NOT_CONTRACT;
	
	public static Decision getDecision(int i){
		if(i == 0){
			return Decision.CONTRACT_AND_DELAY_START;
		}else if(i == 1){
			return Decision.CONTRACT_AND_FORWARD_END;
		}else if(i == 2){
			return Decision.DO_NOT_CONTRACT;
		}else{
			throw new IllegalArgumentException("Invalid index passed as argument");
		}
	}
	
	public static int getIndexOfDecision(Decision dec){
		switch(dec){
		case CONTRACT_AND_DELAY_START:
			return 0;
		case CONTRACT_AND_FORWARD_END:
			return 1;
		case DO_NOT_CONTRACT:
			return 2;	
		default:
			return -1;  //this should never happen
		}
	}

}
