package uvsq.informatique.ter.planification.optimization;

import java.io.IOException;
import java.util.ArrayList;

import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;
import uvsq.informatique.ter.planification.data.csv.ResultOutput;
import uvsq.informatique.ter.planification.data.csv.ResultOutputCSV;

public class AntColony {
	
	private final ArrayList<Task> plannedTasks;
	private final ArrayList<Ant> ants = new ArrayList<Ant>();
	private Ant bestFoundSolution;
	private double bestFoundMeanOccupancyRate = 0.0;
	private final Resource targetResource;
	double alpha = 1.0;
	double beta = 5.0;
	double p = 0.2; //evaporation rate
	ResultOutput output;
	/*
	 * Ants operate with deep copies of schedule in order to not to change the values
	 * during evaluation
	 */
	public AntColony(ArrayList<Task> tasks, int numberOfAnts, Resource targetResource, String outputFilename) throws IOException{
		//this.numberOfAnts = numberOfAnts;
		this.plannedTasks = tasks;
		this.targetResource = targetResource;
		for(int i = 0; i <= numberOfAnts; i++){
			Ant ant = new Ant(tasks, alpha, beta);
			ants.add(ant);
		}
		for(Task task : plannedTasks){
			task.setHeuristicsMarks(targetResource);
		}
		setOutput(outputFilename);
		output.writeHeuristicMarks(plannedTasks);
	}
	
	public void setOutput(String filename){
		output = new ResultOutput(filename);
	}
	
	private void generateSolutions(){
		for(Ant ant : ants){
			ant.generateSolution();
		}
	}
	
	private void updateBestFoundSolution() throws IOException{
		for(Ant ant : ants){
			double res = getSolutionMeanOccupancyRate(ant);
			if(bestFoundSolution == null || bestFoundMeanOccupancyRate > res){
				bestFoundSolution = ant;
				bestFoundMeanOccupancyRate = res;
			}
		}
	}
	
	private double getSolutionMeanOccupancyRate(Ant ant) throws IOException{
		double result = 0.0;
		for(Task task : ant.plannedCopy){ 
			task.applyDecision(ant.getDecisionbyTask(task));
			}
		for(Task t : ant.plannedCopy){
			for(Resource ress : t.getEffort().keySet()){
				if(ress.getCompetence().equals(targetResource.getCompetence())){
					return ress.calculateMeanOccupancyRatePerDay();
				}		
			}
		}
		return result;
	}
	
	private void updatePheromoneInformation(){
		for(Task task : plannedTasks){
			task.updatePheromoneMarks(bestFoundSolution.getDecisionbyTask(task), p, bestFoundMeanOccupancyRate);
		}
	}
	
	private void evaporatePheromoneInformation(){
		for(Task task : plannedTasks){
			task.evaporatePheromone(p);
		}
	}
	
	public void doACO(int nbOfIterations) throws IOException{
		int n = 0;
		while(n < nbOfIterations){
			System.out.println("ACO iteration #: " + n);
			generateSolutions();
			updateBestFoundSolution();
			evaporatePheromoneInformation();
			updatePheromoneInformation();
			System.out.println("Occupancy rate: " + getBestSolutionOccupancyRate() + "%\n");
			n += 1;
		}
		output.writeBestFoundSolution(bestFoundSolution.getSolution(), getBestSolutionOccupancyRate());
	}
	
	public Ant getBestSolution(){
		return bestFoundSolution;
	}
	
	private double getBestSolutionOccupancyRate(){
		return ((targetResource.initial_availability_per_day - bestFoundMeanOccupancyRate) /
				targetResource.initial_availability_per_day) * 100;
	}
}

