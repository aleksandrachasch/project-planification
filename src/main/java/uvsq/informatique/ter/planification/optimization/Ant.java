package uvsq.informatique.ter.planification.optimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;

import uvsq.informatique.ter.planification.core.Resource;
import uvsq.informatique.ter.planification.core.Task;

public class Ant {
	
	public final ArrayList<Task> planned;
	public ArrayList<Task> plannedCopy;
	Map<Task, Decision> solution = new HashMap<Task, Decision>();
	private final double alpha;
	private final double beta;
	
	public Ant(ArrayList<Task> planned, double alpha, double beta){
		this.planned = planned;
		this.plannedCopy = SerializationUtils.clone(planned);
		this.alpha = alpha;
		this.beta = beta;
	}
	
	public void generateSolution(){
		for(Task task : planned){
			double[] probabilities = new double[3];
			double sum = 0.0;
			for(int n = 0; n <3; n++){
				double val = (Math.pow(task.getPheromoneMarks()[n], alpha))*Math.pow(task.getHeuristicsMarks()[n], beta);
				sum += val;
			}
			for(int i = 0; i < 3; i++){
				probabilities[i] = (Math.pow(task.getPheromoneMarks()[i], alpha)*
						Math.pow(task.getHeuristicsMarks()[i], beta)) / sum;
			}
			int i = pickUpADecision(probabilities);
			solution.put(task, Decision.getDecision(i));
		}
	}
	
	public int pickUpADecision(double[] probabilities){
		int result = 0;
		double[] normalized = new double[probabilities.length];
		double sum = 0.0;
		for(int i = 0; i < probabilities.length; i++){
			sum += probabilities[i];
		}
		for(int n = 0; n < normalized.length; n++){
			normalized[n] = probabilities[n] / sum;
		}
		//System.out.println("Normalized: " + Arrays.toString(normalized));
		
		double p = Math.random();
		double cumulativeProbability = 0.0;
		for(int x = 0; x < normalized.length; x++){
			cumulativeProbability += normalized[x];
			if(p < cumulativeProbability){
				//System.out.println(x);
				return x;
			}
		}
		return result;
	}
	
	public Map<Task, Decision> getSolution(){
		return solution;
	}
	
	public Decision getDecisionbyTask(Task task){
		for(Task solutionTask : solution.keySet()){
			if(task.equals(solutionTask)){
				return solution.get(solutionTask);
			}
		}
		throw new IllegalArgumentException("Task does not exist!");
	}

}
